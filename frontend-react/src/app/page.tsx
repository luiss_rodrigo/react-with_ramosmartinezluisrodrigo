"use client";


import React from 'react';
import Primer from './Components/Primer';
import Button from './Components/Button';
import Styles from './Components/Primer.module.css';
import Images from './Components/Images';
import LinkText from './Components/LinkText';
import ImageContainer from './Components/ImageContainer';
import Footer from './Components/footer';






const HomePage: React.FC = () => {
  const handleButtonClick = () => {
    alert('Botón clickeado!');
  };

  return (
    <main>
  
     <div className={Styles.container}>
      <Button onClick={handleButtonClick} text="Boost your productivity" />
      <Primer
        title="A more effective way " 
        paragraph="Effortlessly turn your 
        ideas into a fully functional, 
        responsive, no-code SaaS website 
        in just minutes with the set of free components for Framer." 
        titleClassName={Styles.app}
        paragraphClassName={Styles.app2}
      />
  
     <div>
     <ImageContainer
        mainImageSrc="/Images/part1.svg"
        overlayImageSrc1="/Images/tortus.svg"
        overlayImageSrc2="/Images/pir.svg"
        mainAlt="Main Image Alt Text"
        overlayAlt1="Overlay Image 1 Alt Text"
        overlayAlt2="Overlay Image 2 Alt Text"
      />
    </div>


    <div className={Styles.cont2}>

      <div className={Styles.cont3}>
      <Images src="/Images/hoja.svg" alt="Descriptive Alt Text" className={Styles.part10} />
      <Primer
        title="Integration ecosystem " 
        paragraph="Track your progress and motivate " 
        titleClassName={Styles.cat}
        paragraphClassName={Styles.cat1}
      />
      <LinkText 
        text="Learn more"
        url="https://example.com"
        className={Styles.link}
      />
       <Images src="/Images/fl.svg" alt="Descriptive Alt Text" className={Styles.fl} />
      </div>


      <div className={Styles.cont3}>
      <Images src="Images/other.svg" alt="Descriptive Alt Text" className={Styles.part10} />
      <Primer
        title="Goal setting and tracking" 
        paragraph="Set and track goals with " 
        titleClassName={Styles.cat}
        paragraphClassName={Styles.cat2}
      />
      <LinkText 
        text="Learn more"
        url="https://example.com"
        className={Styles.link}
      />
       <Images src="/Images/fl.svg" alt="Descriptive Alt Text" className={Styles.fl} />
      </div>


      <div className={Styles.cont3}>
      <Images src="Images/cand.svg" alt="Descriptive Alt Text" className={Styles.part10} />
      <Primer
        title="Secure data encryption " 
        paragraph="Ensure your data’s safety with top- tier encryption. " 
        titleClassName={Styles.cat}
        paragraphClassName={Styles.cat3}
      />
      <LinkText 
        text="Learn more"
        url="https://example.com"
        className={Styles.link}
      />
       <Images src="/Images/fl.svg" alt="Descriptive Alt Text" className={Styles.fl} />
      </div>


      <div className={Styles.cont3}>
      <Images src="/Images/not.svg" alt="Descriptive Alt Text" className={Styles.part10} />
      <Primer
        title="Customizable notifications " 
        paragraph="Get alerts on tasks and deadlines that matter most. " 
        titleClassName={Styles.cat}
        paragraphClassName={Styles.cat4}
      />
      <LinkText 
        text="Learn more"
        url="https://example.com"
        className={Styles.link}
      />
       <Images src="/Images/fl.svg" alt="Descriptive Alt Text" className={Styles.fl} />
      </div>

     </div>
    </div>


    <div className={Styles.container2}>
    <Button onClick={handleButtonClick} text="Everything you need" />
      <Primer
        title="Streamlined for easy " 
        paragraph="Enjoy customizable lists, team work tools, and smart tracking all in one place. Set tasks, get reminders, and see your progress simply and quickly." 
        titleClassName={Styles.abb}
        paragraphClassName={Styles.app2}
      />
    </div>
    <div className={Styles.cards}>
      <div className={Styles.card1}>
      <Images src="/Images/card1.svg" alt="Descriptive Alt Text" className={Styles.cubo} />
      <Primer
        title="Integration ecosystem " 
        paragraph="Enhance your productivity by connecting with your favorite tools, keeping all your
        essentials in one place. " 
        titleClassName={Styles.cardTi1}
        paragraphClassName={Styles.cardPa1}
      />
      </div>
      <div className={Styles.card2}>
      <Images src="/Images/card2.svg" alt="Descriptive Alt Text" className={Styles.cubo} />
      <Primer
        title="Goal setting and tracking" 
        paragraph="Define and track your goals, breaking down objectives into achievable tasks to keep your targets in sight." 
        titleClassName={Styles.cardTi1}
        paragraphClassName={Styles.cardPa1}
      />
      </div>
    </div>



    <div className={Styles.footer}>
      <div className={Styles.walk}>
       <div className={Styles.tituloft}>
       <Images src="/Images/foot.svg" alt="Descriptive Alt Text" className={Styles.fotim} />
       <Primer
        title="" 
        paragraph="Effortlessly turn your ideas into a fully functional, responsive, no-code SaaS website. " 
        titleClassName={Styles.fot}
        paragraphClassName={Styles.fot1}
      />
      </div>
        <div className={Styles.iconos6}>
        <Images src="/Images/foot1.svg" alt="Descriptive Alt Text" className={Styles.foticn} />
        <Images src="/Images/foot2.svg" alt="Descriptive Alt Text" className={Styles.foticn} />
        <Images src="/Images/foot3.svg" alt="Descriptive Alt Text" className={Styles.foticn} />
        <Images src="/Images/foot4.svg" alt="Descriptive Alt Text" className={Styles.foticn} />
        <Images src="/Images/foot5.svg" alt="Descriptive Alt Text" className={Styles.foticn} />
        <Images src="/Images/foot6.svg" alt="Descriptive Alt Text" className={Styles.foticn} />
        </div>
        </div>
      <div className={Styles.colum}>
         <div className={Styles.colum2}>
         <Footer
        title="Product"
        paragraphs={[
          "Features",
          "Integrations",
          "Updates",
          "FAQ",
          "Pricing"
        ]}
        titleClassName={Styles.fot4}
        paragraphClassName={Styles.fot5}
        />
         </div>
         <div className={Styles.colum2}>
         <Footer
        title="Company"
        paragraphs={[
          "About",
          "Blog",
          "Careers",
          "Manifesto",
          "Press",
          "Contact"
        ]}
        titleClassName={Styles.fot4}
        paragraphClassName={Styles.fot5}
        />
         </div>
         <div className={Styles.colum2}>
         <Footer
        title="Resources"
        paragraphs={[
          "Examples",
          "Community",
          "Guides",
          "Docs"
        ]}
        titleClassName={Styles.fot4}
        paragraphClassName={Styles.fot5}
        />
         </div>
         <div className={Styles.colum3}>
         <Footer
        title="Legal"
        paragraphs={[
          "Privacy",
          "Terms",
          "Security"
        ]}
        titleClassName={Styles.fot4}
        paragraphClassName={Styles.fot5}
        />
         </div>
      </div>
    </div>
    </main>

  );
};

export default HomePage;
