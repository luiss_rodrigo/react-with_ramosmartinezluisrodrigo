
import React from 'react';
import styles from './Primer.module.css';

interface ImageContainerProps {
  mainImageSrc: string;
  overlayImageSrc1: string;
  overlayImageSrc2: string;
  mainAlt: string;
  overlayAlt1: string;
  overlayAlt2: string;
}

const ImageContainer: React.FC<ImageContainerProps> = ({
  mainImageSrc,
  overlayImageSrc1,
  overlayImageSrc2,
  mainAlt,
  overlayAlt1,
  overlayAlt2,
}) => {
  return (
    <div className={styles.imageContainer}>
      <img src={mainImageSrc} alt={mainAlt} className={styles.mainImage} />
      <img src={overlayImageSrc1} alt={overlayAlt1} className={styles.overlayImage1} />
      <img src={overlayImageSrc2} alt={overlayAlt2} className={styles.overlayImage2} />
    </div>
  );
};

export default ImageContainer;
