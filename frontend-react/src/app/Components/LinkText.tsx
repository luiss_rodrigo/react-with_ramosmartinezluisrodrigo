
import React from 'react';

interface LinkTextProps {
  text: string;
  url: string;
  className?: string;
}

const LinkText: React.FC<LinkTextProps> = ({ text, url, className }) => {
  return (
    <a href={url} className={className} target="_blank" rel="noopener noreferrer">
      {text}
    </a>
  );
};

export default LinkText;
