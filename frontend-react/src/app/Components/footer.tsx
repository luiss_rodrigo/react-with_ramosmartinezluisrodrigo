"use client";

import React from 'react';
import styles from './Primer.module.css';

interface Footer {
  title: string;
  paragraphs: string[];
  titleClassName?: string;
  paragraphClassName?: string;
}

const Footer: React.FC<Footer> = ({ title, paragraphs, titleClassName, paragraphClassName }) => {
  return (
    <div>
      <h4 className={titleClassName || styles.title}>{title}</h4>
      {paragraphs.map((paragraph, index) => (
        <p key={index} className={paragraphClassName || styles.paragraph}>{paragraph}</p>
      ))}
    </div>
  );
};

export default Footer;
