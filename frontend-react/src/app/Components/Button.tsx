"use client";


import React from 'react';
import Styles from './Primer.module.css';


interface ButtonProps {
  onClick: () => void;
  text: string;
}

const Button: React.FC<ButtonProps> = ({ onClick, text }) => {
  return (
    <button className={Styles.button} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;
