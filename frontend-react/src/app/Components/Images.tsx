import React from 'react';

interface ImagesProps {
  src: string;
  alt: string;
  className?: string;
}

const Images: React.FC<ImagesProps> = ({ src, alt, className }) => {
  return (
    <img src={src} alt={alt} className={className}/>
  );
};

export default Images;