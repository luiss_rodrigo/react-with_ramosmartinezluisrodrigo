
"use client";


import React from 'react';
import styles from './Primer.module.css';

interface PrimerProps {
  title: string;
  paragraph: string;
  titleClassName?: string;
  paragraphClassName?: string;
}

const Primer: React.FC<PrimerProps> = ({ title, paragraph, titleClassName, paragraphClassName }) => {
  return (
    <div>
      <h1 className={titleClassName || styles.title}>{title}</h1>
      <p className={paragraphClassName || styles.paragraph}>{paragraph}</p>
    </div>
  );
};

export default Primer;
